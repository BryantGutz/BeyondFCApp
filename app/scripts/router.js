/* global define: false */

define([
	'jquery',
	'underscore',
	'backbone',
	'views/AppView',
	'views/HomeView',
	'views/ClubView',
	'views/TeamsView',
	'views/FirstTeamView',
	'views/ReservesTeamView',
	'views/MetroTeamView',
	'views/MembershipView'
],function($, _, Backbone, AppView, HomeView, ClubView, TeamsView, FirstTeamView, ReservesTeamView, MetroTeamView, MembershipView) {
		'use strict';

		var AppRouter = Backbone.Router.extend({
			routes: {
				'join/:subsection' : 'loadMembership',
				'join' : 'loadMembership',
				'team/metro' : 'loadMetroTeam',
				'team/reserve' : 'loadReservesTeam',
				'team/first' : 'loadFirstTeam',
				// 'team' : 'loadTeam',
				'teams/:subSection' : 'loadTeams',
				'teams' : 'loadTeams',
				'club/:subSection': 'loadClub',
				'club': 'loadClub',
				'home': 'loadHome',
				'*actions': 'loadHome'
			}
		});

		var initialize = function() {

			var appRouter = new AppRouter(),
				appView = AppView;
			
			$('body').prepend( appView.render().el );

			appRouter.on('route:loadHome', function() {
				replaceMainView( HomeView, 'home', { parent: appView, mainClass: 'main-home' } );
			});
			appRouter.on('route:loadClub', function(subSection) {
				replaceMainView( ClubView, 'club', { parent: appView, subSection: subSection, mainClass: 'main-club' });
			});
			appRouter.on('route:loadMembership', function(subSection) {
				replaceMainView( MembershipView, 'membership', { parent: appView, subSection: subSection, mainClass: 'main-membership' });
			});
			appRouter.on('route:loadTeams', function(subSection) {
				replaceMainView( TeamsView, 'teams', { parent: appView, subSection: subSection, mainClass: 'main-teams' } );
			});
			appRouter.on('route:loadFirstTeam', function() {
				replaceMainView( FirstTeamView, 'team', { parent: appView, mainClass: 'main-team' } );
			});
			appRouter.on('route:loadReservesTeam', function() {
				replaceMainView( ReservesTeamView, 'team', { parent: appView, mainClass: 'main-team' } );
			});
			appRouter.on('route:loadMetroTeam', function() {
				replaceMainView( MetroTeamView, 'team', { parent: appView, mainClass: 'main-team' } );
			});


			var appendNewView = function(NewView, mainViewName, options) {
				appView.mainViewName = mainViewName;
				appView.currView = new NewView(options);
				appView.$mainContent.empty().removeClass().addClass(options.mainClass).html( appView.currView.render().el ).fadeIn(function(){
					appView.currView.preload();
					appView.currView.trigger('navToSection', options.subSection);	
				});
				
				
			};

			var replaceMainView = function(NewView, mainViewName, options) {
				// var page = Backbone.history.fragment || '/';
				if (appView.mainViewName !== mainViewName) {
					appView.trigger('navChange', Backbone.history.fragment);
					if (appView.currView) {
						appView.$mainContent.fadeOut(function() {
							appView.currView.close();
							appendNewView(NewView, mainViewName, options);
						});
					}
					else {
						appendNewView(NewView, mainViewName, options);
					}
				}
				else {
					appView.currView.trigger('navToSection', options.subSection);
				}

				// ga('send', 'pageview', page);
			};

			Backbone.history.start();

			
		};

		return {
			initialize: initialize
		};
}
);