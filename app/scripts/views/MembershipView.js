/* global define */

define([
	'jquery',
	'underscore',
	'backbone',
	'channel',
	'text!templates/membership.html'
],function($, _, Backbone, channel, membershipTemplate) {
		'use strict';
		var MembershipView = Backbone.View.extend({

			className: 'membership page',

			template: _.template(membershipTemplate),

			initialize: function() {
				this.on('navOpen', this.onNavOpen);
				this.on('navClose', this.onNavClose);
				this.on('navToSection', this.navToSection);
				// this.parent = this.options.parent;
				// this.navEl = this.options.navEl;
			},
			render: function(){
				//this.$el.empty();
				this.$el.append( this.template ).fadeIn();
				this.bindings();
				return this;
			},
			navToSection: function(sectionId){
				
				var target = $('#'+sectionId);
				if (target.length) {
					$( document ).ready(function() {
   						$('html, body').animate({
		            		scrollTop: target.offset().top
		          		}, 1000);
					});
		        }	
		        else{
		        	$('html, body').animate({
		            		scrollTop: 0
		          	}, 1000);
		        }
			},


			bindings: function() {
			},

			onNavOpen: function() {
			
			},

			onNavClose: function() {
			
			}

		});
		// Our module now returns our view
		return MembershipView;
	}
);