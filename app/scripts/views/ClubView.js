/* global define, Modernizr: false */

define([
	'jquery',
	'underscore',
	'backbone',
	'channel',
	'text!templates/club.html'
],function($, _, Backbone, channel, clubTemplate) {
		'use strict';
		var ClubView = Backbone.View.extend({

			className: 'club page',

			template: _.template(clubTemplate),

			events: function() {
				return Modernizr.touch ? {
					'touch .contactBtn': 'goToContact'
				}:{
					'click .contactBtn': 'goToContact'
				};
			},

			initialize: function() {
				this.on('navOpen', this.onNavOpen);
				this.on('navClose', this.onNavClose);
				this.on('navToSection', this.navToSection);
				// this.parent = this.options.parent;
				// this.navEl = this.options.navEl;
			},
			render: function(){
				//this.$el.empty();
				this.$el.append( this.template ).fadeIn();
				this.bindings();
				return this;
			},

			bindings: function() {
				
			},

			onNavOpen: function() {

			},

			onNavClose: function() {
			},
			navToSection: function(sectionId){
				
				var target = $('#'+sectionId);
				if (target.length) {
					$( document ).ready(function() {
   						$('html, body').animate({
		            		scrollTop: target.offset().top
		          		}, 1000);
					});
		        }	
		        else{
		        	$('html, body').animate({
		            		scrollTop: 0
		          	}, 1000);
		        }
			},
			goToContact: function(){
				this.navToSection('contact-us');
			}

		});
		// Our module now returns our view
		return ClubView;
	}
);