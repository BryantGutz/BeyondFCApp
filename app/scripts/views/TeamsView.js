/* global define */

define([
	'jquery',
	'underscore',
	'backbone',
	'channel',
	'text!templates/teams.html'
],function($, _, Backbone, channel, teamsTemplate) {
		'use strict';
		var TeamsView = Backbone.View.extend({

			className: 'team page',

			template: _.template(teamsTemplate),

			initialize: function() {
				this.on('navToSection', this.navToSection);
				// this.parent = this.options.parent;
				// this.navEl = this.options.navEl;
			},
			
			render: function(){
				//this.$el.empty();
				this.$el.append( this.template ).fadeIn();
				this.bindings();
				return this;
			},

			navToSection: function(sectionId){
				
				var target = $('#'+sectionId);
				if (target.length) {
					$( document ).ready(function() {
   						$('html, body').animate({
		            		scrollTop: target.offset().top
		          		}, 1000);
					});
		        }	
		        else{
		        	$('html, body').animate({
		            		scrollTop: 0
		          	}, 1000);
		        }
			},
			bindings: function() {
			},

			onNavOpen: function() {
			},

			onNavClose: function() {
			}
		});
		// Our module now returns our view
		return TeamsView;
	}
);