/* global define: false, Modernizr: false */

define([
	'jquery',
	'underscore',
	'backbone',
	'text!templates/app.html'
],function($, _, Backbone, appTemplate) {
		'use strict';
		var AppView = Backbone.View.extend({


			events: function() {
				return Modernizr.touch ? {
					'click .main-nav li': 'removeNav'
				}:{
					'click .main-nav li': 'removeNav'
				};
			},

			overlayOn: true,

			template: _.template(appTemplate),

			navOpen: false,

			initialize: function() {
				_.bindAll(this);
				// this.on('navChange', this.setCurrentNav);
				window.IS_MOBILE = (function() {
					return navigator.userAgent.match(/Android|BlackBerry|iPhone|iPod|Opera Mini|IEMobile/i)?true:false;
				})();
			},

			render: function() {
				this.$el.append( this.template );
				this.bindings();
				this.preloadImages();
				this.replaceSVG();
				return this;
			},

			preloadImages: function() {
				// this.preloader = new ImagePreloader({
				// 	urls: ['images/ajax-loader-md-grey.gif','images/ajax-loader.gif']
				// });
				// this.preloader.start();
			},

			bindings: function() {
				this.$nav = this.$('.main-nav');
				this.$mainContent = this.$('#main-content');
			},

			toggleNav: function() {
				this.$nav.toggleClass('active');
				if (this.navOpen) {
					this.navOpen = false;
					this.currView.trigger('navClose');
				}
				else {
					this.navOpen = true;
					this.currView.trigger('navOpen');
				}
			},

			removeNav: function() {
				this.navOpen = false;
				this.$nav.removeClass('active');
				this.currView.trigger('navClose');
			},

			setCurrentNav: function(navEl) {
				navEl = navEl.split('/');
				// if (navEl[0].length === 0 ) {
				// 	// navEl = ['home'];
				// }
				// this.$navEls[navEl[0]].addClass('current').siblings().removeClass('current');
			}
		});
		// Our module now returns our view
		return new AppView();
	}
);