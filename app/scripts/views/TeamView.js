/* global define */

define([
	'jquery',
	'underscore',
	'backbone',
	'channel',
	'text!json/first.json',
	'text!templates/team.html',
	'text!templates/team-player.html'
],function($, _, Backbone, channel, teamJson, teamTemplate, playerTemplate) {
		'use strict';
		var TeamView = Backbone.View.extend({

			className: 'team page',

			template: _.template(teamTemplate),

			initialize: function() {
				this.on('navOpen', this.onNavOpen);
				this.on('navClose', this.onNavClose);
				
			},
			render: function(){
				//this.$el.empty();
				this.$el.append( this.template ).fadeIn();
				this.bindings();
				this.renderList();
				return this;
			},

			bindings: function() {
				this.$playerContainer = this.$('.team');
				this.playerList = JSON.parse(teamJson);
				
			},

			onNavOpen: function() {
			},

			onNavClose: function() {
			},

			renderList: function() {

				var $players = $('<div class="row" />');
				
				for (var i = 0, len = this.playerList.list.length; i < len; i++) {
					var template = _.template(playerTemplate, this.playerList.list[i]);
					$players.append(template);
				}
				this.$playerContainer.append($players);
			}
		});
		// Our module now returns our view
		return TeamView;
	}
);