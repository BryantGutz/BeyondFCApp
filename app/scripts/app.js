/* global define: false, Modernizr: false */

define(
	['jquery','underscore','backbone','router'],
	function($, _, Backbone, Router) {
		'use strict';
		var initialize = function() {
			Backbone.View.prototype.close = function() {
				this.remove();
				this.unbind();
				if (this.onClose){
					this.onClose();
				}
			};
			Backbone.View.prototype.preload = function() {
				this.$el.fadeIn();
			};
			Backbone.View.prototype.replaceSVG = function() {
				if (!Modernizr.inlinesvg) {
					var images = this.$('img');
					$.each(images, function(i, obj) {
						var png = $(obj).attr('data-png');
						if (typeof png !== 'undefined') {
							$(obj).attr('src', png);
						}
					});
				}
			};
			Router.initialize();
		};

		return { initialize: initialize };
	}
);