/*global define: false */

define(['underscore','backbone'], function (_, Backbone) {
	'use strict';
    var channel = _.extend({}, Backbone.Events);
    return channel;
});